//jquery for filtering input
$(document).ready(function(){
    $("#input").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#categoryTable tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
});

main();

async function main (){
    const categories = await getCategories()
    
    var str = "";
    //populate table
    for (var i = 0; i < categories.length; i++){
        str += "<tr><td>" + categories[i] +"</td></tr>"
    }

    document.getElementById("categoryTable").innerHTML = str;
}

//this function fetches category data into JSON array
async function getCategories() {
    await window.fetch('https://api.publicapis.org/categories').then(res => res.json()).then((response) =>    {
        result = response;
    });
    return result;
}

